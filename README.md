## Summary
* Listens for mouse clicks and tells OBS to record the last N seconds of footage when the click is detected.
* Useful to record your shots in a game and replay for analysis.
* Set APPLICATION_NAME to an exact match exe to only trigger the OBS recording when that app is focused.
* Set DELETE_REPLAYS_ON_START to delete replays in REPLAYS_DIRECTORY older than MINIMUM_AGE_TO_DELETE_SECONDS.
* CLICK_DEBOUNCE_SECONDS just avoids triggering on every click, essentially only capturing one click every (default:2) seconds

## Installing
pip install -r requirements.txt

## Setup
1. Setup OBS how you would normally record (audio, display or window capture, etc)
2. File > Settings > Output : [Check] Enable Replay Buffer
3. Set Maximum Replay Time to match BUFFER_LENGTH_SECONDS variable (default 10sec)
4. Tools > WebSocket Server Settings
5. Enable WebSocket Server
6. Port : 4455, [Check] Enable Authentication, Type a password
7. Create the file in C:\Users\{username}\config.toml
8. Match the contents of the file below, replacing your password

```
[connection]
host = "localhost"
port = 4455
password = "password"
```

9. File > Show Recordings to see your videos

## Usage
python ./program.py