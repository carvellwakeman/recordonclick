from pynput import mouse
import obsws_python as obs
from datetime import datetime,timedelta
import threading
import win32gui
import win32process
import psutil
import os
import re


CLICK_DEBOUNCE_SECONDS = 2
BUFFER_LENGTH_SECONDS = 10
APPLICATION_NAME = "HuntGame.exe" # Set to None for any
DELETE_REPLAYS_ON_START = True
REPLAYS_DIRECTORY = "C:/Users/Vega/Videos"
MINIMUM_AGE_TO_DELETE_SECONDS = 86400 #24hr

mutex = threading.Lock()
last_activity = datetime.now()
last_replay = datetime.now()-timedelta(seconds=BUFFER_LENGTH_SECONDS)

cl = obs.ReqClient() # C:\Users\{username}\config.toml (see below)
#[connection]
#host = "localhost"
#port = 4455
#password = "password"

# https://stackoverflow.com/a/47936739
def active_window_process_name():
    pid = win32process.GetWindowThreadProcessId(win32gui.GetForegroundWindow()) #This produces a list of PIDs active window relates to
    return psutil.Process(pid[-1]).name() #pid[-1] is the most likely to survive last longer

def delete_replays():
    print(f"Looking for replays older than {MINIMUM_AGE_TO_DELETE_SECONDS/60/60} hours to delete")
    regex = re.compile("(Replay.*mkv$)")
    for root, dirs, files in os.walk(REPLAYS_DIRECTORY):
        for file in files:
            if regex.match(file):
                fileDate = datetime.fromtimestamp(os.path.getctime(REPLAYS_DIRECTORY+"/"+file))
                if ((datetime.now()-fileDate).seconds >= MINIMUM_AGE_TO_DELETE_SECONDS):
                    print(f"Removing {file}")
                    os.remove(file)
    print("Done.")
            
def on_click(x, y, button, pressed):
    global last_activity
    if pressed and str(button) == "Button.left":
        print(f"Left Click at {x}, {y}")

        if (APPLICATION_NAME is None or APPLICATION_NAME in active_window_process_name()):
            # Round up to the next BUFFER_LENGTH_SECONDS window
            if (last_replay > datetime.now()-timedelta(seconds=BUFFER_LENGTH_SECONDS)):
                thread_wait = BUFFER_LENGTH_SECONDS-(datetime.now()-last_replay).seconds
            else:
                thread_wait = CLICK_DEBOUNCE_SECONDS

            # Don't records unless the last activity was more than debounce ago
            if (last_activity < datetime.now()-timedelta(seconds=CLICK_DEBOUNCE_SECONDS)):
                print(f"Record last {BUFFER_LENGTH_SECONDS} seconds in {thread_wait} seconds")
                threading.Timer(thread_wait, try_save).start()
                with mutex:
                    last_activity = datetime.now()
        else:
            print("Ignoring, target application mismatch")
    
    if pressed and str(button) == "Button.right":
        print(f"Right Click at {x}, {y}")

    
def try_save():
    global last_activity, last_replay
    with mutex:
        # Check debounce
        if (last_activity < datetime.now()-timedelta(seconds=CLICK_DEBOUNCE_SECONDS)):
            # Last replay was more than the buffer length ago
            if (last_replay <= datetime.now()-timedelta(seconds=BUFFER_LENGTH_SECONDS)):
                print(f"Saved last {BUFFER_LENGTH_SECONDS} seconds")
                save_replay()
                last_replay = datetime.now()
            else:
                print("Fail due to buffer window") # Shouldn't happen if scheduling is correct
        # else:
        #     print("Fail due to debounce")

def get_replay_buffer_status():
    resp = cl.send("GetReplayBufferStatus", raw=True)
    return resp["outputActive"]

def start_replay_buffer():
    if (not get_replay_buffer_status()):
        cl.send("StartReplayBuffer", raw=True)
        return True
    return False

def stop_replay_buffer():
    if (get_replay_buffer_status()):
        cl.send("StopReplayBuffer", raw=True)
        return True
    return False

def save_replay():
    if (get_replay_buffer_status()):
        cl.send("SaveReplayBuffer", raw=True)


if (DELETE_REPLAYS_ON_START):
    delete_replays()

start_replay_buffer()

with mouse.Listener(on_click = on_click) as listener:
    listener.join()

stop_replay_buffer()
